import { Pipe, PipeTransform, Sanitizer } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { prependBackendUrl } from "src/app/lib/helpers/files.helper";

@Pipe({
  name: "sanitizeStyleUrl"
})
export class SanitizeStyleUrlPipe implements PipeTransform {
  constructor(private sanitizier: DomSanitizer) {}
  transform(url: string, [prepend = true] = []) {
    return this.sanitizier.bypassSecurityTrustStyle(
      `url(${prependBackendUrl(url)})`
    );
  }
}
