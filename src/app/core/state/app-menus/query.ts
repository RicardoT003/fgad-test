import { QueryEntity, QueryConfig, Order } from "@datorama/akita";
import { Injectable } from "@angular/core";
import { combineLatest } from "rxjs";
import { map } from "rxjs/operators";

import { Menu } from "src/app/domain/models/menu.model";
import { sortBy } from "src/app/lib/helpers/array.helper";
import { AppMenusState } from "./state";
import { AppMenusStore } from "./store";

@Injectable({
  providedIn: "root"
})
@QueryConfig({
  sortBy: "orden"
})
export class AppMenusQuery extends QueryEntity<AppMenusState> {
  menus$ = this.selectAll();
  navigationMenus$ = combineLatest(
    this.selectAll(),
    this.selectAll({ asObject: true })
  ).pipe(
    map(([menus]) => {
      const rootMenus = menus.filter(m => !m.menuPadre);
      const rootMenusWithSubmenus = rootMenus.map(root => {
        return new Menu({
          ...root,
          subMenus: root.subMenus
            .filter(sub => sub.mostrarEnNavbar)
            .sort(sortBy("orden"))
        });
      });
      return rootMenusWithSubmenus;
    })
  );
  treeMenu$ = combineLatest(
    this.selectAll(),
    this.selectAll({ asObject: true })
  ).pipe(
    map(([menus, menusDict]) => {
      const menusCopy: Menu[] = JSON.parse(JSON.stringify(menus));
      const menusDictCopy: { [id: string]: Menu } = JSON.parse(
        JSON.stringify(menusDict)
      );
      const rootMenus = menusCopy.filter(m => !m.menuPadre);
      rootMenus.forEach(root => {
        const subMenus = root.subMenus.map(sub => menusDictCopy[sub.id]);
        subMenus.forEach(subMenu => {
          const subSubMenus = subMenu.subMenus.map(
            sub => menusDictCopy[sub.id]
          );
          subMenu.subMenus = subSubMenus;
        });
        root.subMenus = subMenus;
      });
      return rootMenus;
    })
  );
  get activeMenuCode() {
    return this.getValue().ui.activatedMenuCode;
  }
  activatedMenuCode$ = this.select(state => state.ui.activatedMenuCode);
  activatedSubMenuCode$ = this.select(state => state.ui.activatedSubmenuCode);
  activatedSubMenu2Code$ = this.select(state => state.ui.activatedSubmenu2Code);

  getByCode(code: string): Menu {
    return this.getAll({
      filterBy: m => m.codigo === code
    })[0];
  }

  getSubmenus(code: string): Menu[] {
    return this.getAll({
      filterBy: m => !!m.menuPadre && m.menuPadre.codigo === code
    });
  }

  constructor(protected appMenusStore: AppMenusStore) {
    super(appMenusStore);
  }
}
