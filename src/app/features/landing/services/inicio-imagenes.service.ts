import { Injectable } from "@angular/core";
import { InicioImagenesStore } from "../state/inicio-imagenes/store";
import { ApiUtil } from "src/app/core/utils/api.util";
import { tap, catchError } from "rxjs/operators";
import { InicioImagen } from "../domain/inicio-imagen.model";
import { of } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class InicioImagenesService {
  constructor(
    private inicioImagenesStore: InicioImagenesStore,
    private api: ApiUtil
  ) {}

  loadInicioImagenes() {
    return this.api
      .get("/inicioimagens?activo=1", { isAuthenticated: false })
      .pipe(
        tap((resp) => console.log("RESP", resp)),
        tap((resp) => this.inicioImagenesStore.set(resp)),
        catchError((err) => of(this.inicioImagenesStore.setError(err)))
      );
  }
}
