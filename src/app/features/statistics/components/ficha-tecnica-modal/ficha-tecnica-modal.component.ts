import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FichaTecnica } from '../../models/ficha-tecnica.model';
import { AppConstantsQuery } from 'src/app/core/state/app-constants/query';

@Component({
  selector: 'app-ficha-tecnica-modal',
  templateUrl: './ficha-tecnica-modal.component.html',
  styleUrls: ['./ficha-tecnica-modal.component.scss']
})
export class FichaTecnicaModalComponent implements OnInit {

  fichaTecnica: FichaTecnica;
  fichaTecnicaNombreIndicadorLabel: string;
  fichaTecnicaFormulaIndicadorLabel: string;
  fichaTecnicaDescripcionLabel: string;
  fichaTecnicaUnidadMedidaLabel: string;
  fichaTecnicaFuenteLabel: string;

  constructor(@Inject(MAT_DIALOG_DATA) data: any, constants: AppConstantsQuery) {
    Object.assign(this, data);
    this.fichaTecnicaNombreIndicadorLabel = constants.getByCode('FICHA_TECNICA_LABEL_NOMBRE_INDICADOR').valor;
    this.fichaTecnicaFormulaIndicadorLabel = constants.getByCode('FICHA_TECNICA_LABEL_FORMULA_INDICADOR').valor;
    this.fichaTecnicaDescripcionLabel = constants.getByCode('FICHA_TECNICA_LABEL_DESCRIPCION').valor;
    this.fichaTecnicaUnidadMedidaLabel = constants.getByCode('FICHA_TECNICA_LABEL_UNIDAD_MEDIDA').valor;
    this.fichaTecnicaFuenteLabel = constants.getByCode('FICHA_TECNICA_LABEL_FUENTE').valor;
  }

  ngOnInit() {
  }

}
