import { NgModule } from "@angular/core";
import { LoginComponent } from "./login/login.component";
import { AppSharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [LoginComponent],
  imports: [AppSharedModule],
  exports: [LoginComponent]
})
export class PagesModule {}
