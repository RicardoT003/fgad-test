import { EntityStore, StoreConfig } from '@datorama/akita';
import { AnniosState } from './state';
import { Injectable } from '@angular/core';

@StoreConfig({
    name: 'annios'
})
@Injectable({
    providedIn: 'root'
})
export class AnniosStore extends EntityStore<AnniosState> {
    constructor() {
        super();
    }
}