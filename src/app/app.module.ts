import { NgModule, APP_INITIALIZER } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CoreModule } from "./core/core.module";
import { appInitializer } from "./app.initializer";
import { AppInitializationService } from "./core/services/app-initialization.service";
import { environment } from "src/environments/environment";
import { AkitaNgDevtools } from "@datorama/akita-ngdevtools";

const imports = [AppRoutingModule, CoreModule];

if (!environment.production) {
  AkitaNgDevtools.forRoot();
}

@NgModule({
  declarations: [AppComponent],
  imports: imports,
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializer,
      deps: [AppInitializationService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
