import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategicCommitteeComponent } from './strategic-committee.component';

describe('StrategicCommitteeComponent', () => {
  let component: StrategicCommitteeComponent;
  let fixture: ComponentFixture<StrategicCommitteeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategicCommitteeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategicCommitteeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
