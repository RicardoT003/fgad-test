import { Store, StoreConfig } from "@datorama/akita";
import { AppSessionState } from "./state";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
@StoreConfig({
  name: "session",
  resettable: true
})
export class AppSessionStore extends Store<AppSessionState> {
  constructor() {
    super({
      token: null,
      user: null
    });
  }
}
