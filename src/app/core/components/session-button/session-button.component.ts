import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AppSessionQuery } from '../../state/app-session/query';
import { AppSessionService } from '../../services/app-session.service';
import { AppMenusService } from '../../services/app-menus.service';
import { MatDialog } from '@angular/material/dialog';
import { ChangePasswordDialogComponent } from '../change-password-dialog/change-password-dialog.component';

@Component({
  selector: 'app-session-button',
  templateUrl: './session-button.component.html',
  styleUrls: ['./session-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SessionButtonComponent implements OnInit {

  openAccountOptions = false;

  constructor(
    public appSessionQuery: AppSessionQuery,
    private appSessionSvc: AppSessionService,
    private appMenusSvc: AppMenusService,
    private changeDetectorRef: ChangeDetectorRef,
    private matDialog: MatDialog) { }

  ngOnInit() {
  }

  toggleAccountOptions() {
    this.openAccountOptions = !this.openAccountOptions;
    this.changeDetectorRef.detectChanges();
    setTimeout(() => {
      const menuAreaElm = document.getElementById('navbar__menu-area');
      menuAreaElm.scrollTop = menuAreaElm.scrollHeight;
    }, 0);
  }

  logout() {
    this.appSessionSvc.purgeSession();
    this.appMenusSvc.loadMenus().subscribe();
    this.openAccountOptions = false;
    this.changeDetectorRef.detectChanges();
  }

  changePassword() {
    this.matDialog.open(ChangePasswordDialogComponent, {
      disableClose: true,
      panelClass: 'change-password-dialog'
    });
    this.openAccountOptions = false;
    this.changeDetectorRef.detectChanges();
  }

}
