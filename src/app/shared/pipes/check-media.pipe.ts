import { PipeTransform, Pipe } from '@angular/core';

const BREAKPOINTS = {
    xs: 320,
    sm: 480,
    md: 768,
    lg: 992,
    xl: 1200
}

@Pipe({
    name: 'checkMedia'
})
export class CheckMediaPipe implements PipeTransform {

    transform(breakpoints: string[]): boolean {
        const validBps = Object.keys(BREAKPOINTS).filter(bp => BREAKPOINTS[bp] <= window.innerWidth);
        const currentBp = validBps[validBps.length - 1];
        return breakpoints.indexOf(currentBp) >= 0;
    }
}