import { PipeTransform, Pipe } from '@angular/core';
import { AppMenusQuery } from 'src/app/core/state/app-menus/query';

@Pipe({
    name: 'isMenuActive'
})
export class IsMenuActivePipe implements PipeTransform {
    constructor(private appMenuQuery: AppMenusQuery) {

    }

    transform(menuCode: string): boolean {
        console.log(menuCode, this.appMenuQuery.activeMenuCode);
        return this.appMenuQuery.activeMenuCode === menuCode;
    }
}