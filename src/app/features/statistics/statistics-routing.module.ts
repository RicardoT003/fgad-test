import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PagesModule } from "./pages/pages.module";
import { StatisticsComponent } from "./statistics.component";
import { StrategicInfoComponent } from "./pages/strategic-info/strategic-info.component";
import { SaveMenuStateGuard } from "./guards/save-menu-state.guard";
import { LoadPageGuard } from "./guards/load-page.guard";
import { LoadRequirementsGuard } from "./guards/load-requirements.guard";

const routes: Routes = [
  {
    path: "",
    component: StatisticsComponent,
    canActivate: [LoadRequirementsGuard],
    canActivateChild: [LoadPageGuard],
    children: [
      {
        path: ":menuCode",
        pathMatch: 'full',
        component: StrategicInfoComponent,
        canActivate: [SaveMenuStateGuard]
      },
      {
        path: ":menuCode/:submenuCode",
        pathMatch: 'full',
        component: StrategicInfoComponent,
        canActivate: [SaveMenuStateGuard]
      },
      {
        path: ":menuCode/:submenuCode/:submenu2Code",
        pathMatch: 'full',
        component: StrategicInfoComponent,
        canActivate: [SaveMenuStateGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), PagesModule],
  exports: [RouterModule],
  providers: [SaveMenuStateGuard, LoadPageGuard, LoadRequirementsGuard]
})
export class StatisticsRoutingModule { }
