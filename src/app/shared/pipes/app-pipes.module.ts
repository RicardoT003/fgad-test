import { NgModule } from "@angular/core";
import { PrependBackendurlPipe } from "./prepend-backendurl.pipe";
import { SanitizeStyleUrlPipe } from "./sinitize-style-url.pipe";
import { IsMenuActivePipe } from './is-menu-active.pipe';
import { HtmlFromMarkupPipe } from './html-from-markup.pipe';
import { ValidYearForProjectPipe } from './valid-year-for-project.pipe';
import { CheckMediaPipe } from './check-media.pipe';

const pipes = [PrependBackendurlPipe, SanitizeStyleUrlPipe, IsMenuActivePipe, HtmlFromMarkupPipe, ValidYearForProjectPipe, CheckMediaPipe];

@NgModule({
  declarations: pipes,
  exports: pipes
})
export class AppPipesModule { }
