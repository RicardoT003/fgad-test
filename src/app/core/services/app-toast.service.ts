import { Injectable } from "@angular/core";
declare var iziToast;

@Injectable()
export class AppToastService {
  options = {
    position: "bottomCenter"
  };

  constructor() {}

  success(message, title = "") {
    iziToast.success({
      title,
      message,
      ...this.options
    });
  }

  info(message, title = "") {
    iziToast.info({
      title,
      message,
      ...this.options
    });
  }

  error(message, title = "") {
    iziToast.error({
      title,
      message,
      ...this.options
    });
  }
}
