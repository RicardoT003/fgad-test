import { NgModule } from "@angular/core";
import { AppSharedModule } from "src/app/shared/shared.module";
import { ShellComponent } from "./shell/shell.component";
import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from "./header/header.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { RouterModule } from "@angular/router";
import { SessionButtonComponent } from './session-button/session-button.component';
import { ChangePasswordDialogComponent } from './change-password-dialog/change-password-dialog.component';

@NgModule({
  imports: [AppSharedModule, RouterModule],
  declarations: [
    ShellComponent,
    FooterComponent,
    HeaderComponent,
    NavbarComponent,
    SessionButtonComponent,
    ChangePasswordDialogComponent
  ],
  exports: [ShellComponent],
  entryComponents: [ChangePasswordDialogComponent]
})
export class AppCoreComponentsModule {}
