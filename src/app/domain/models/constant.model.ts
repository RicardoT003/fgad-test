import { BaseModel } from "./base/base.model";

export class Constant extends BaseModel<Constant> {
  codigo: string;
  valor: string;
}
