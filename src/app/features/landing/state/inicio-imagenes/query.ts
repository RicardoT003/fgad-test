import { QueryEntity, QueryConfig, Order } from "@datorama/akita";
import { Injectable } from "@angular/core";

import { InicioImagenesState } from "./state";
import { InicioImagenesStore } from "./store";
import { tap } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
@QueryConfig({
  sortBy: "ordenEnSlider",
  sortByOrder: Order.ASC
})
export class InicioImagenesQuery extends QueryEntity<InicioImagenesState> {
  inicioImagenes$ = this.selectAll();
  loading$ = this.selectLoading();

  constructor(protected store: InicioImagenesStore) {
    super(store);
  }
}
