import { EntityStore, StoreConfig } from "@datorama/akita";
import { ProyectosState } from "./state";
import { Injectable } from "@angular/core";

@StoreConfig({
  name: "proyectos"
})
@Injectable({
  providedIn: "root"
})
export class ProyectosStore extends EntityStore<ProyectosState> {
  constructor() {
    super();
  }
}
