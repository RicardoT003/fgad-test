export interface AppLayoutState {
  showNavbar: boolean;
  headerMode: string;
  pushNavbar: boolean;
  showMobileNavbar: boolean;
}
