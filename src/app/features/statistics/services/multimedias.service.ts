import { Injectable } from "@angular/core";
import { MultimediaStore } from "../state/multimedias/store";
import { MultimediasQuery } from "../state/multimedias/query";
import { ApiUtil } from "src/app/core/utils/api.util";
import { tap, map, catchError } from "rxjs/operators";
import { of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class MultimediasService {
  constructor(
    private multimediasStore: MultimediaStore,
    private multimediasQuery: MultimediasQuery,
    private api: ApiUtil
  ) {}

  loadMultimedias(type: "comite" | "documentoClave") {
    const url = type === "comite" ? "/comitemiembros" : "/documentoclaves";
    if (type === "comite" && this.multimediasQuery.comiteLoaded) {
      return;
    }
    if (
      type === "documentoClave" &&
      this.multimediasQuery.documentosClaveLoaded
    ) {
      return;
    }
    return this.api
      .get(url, { isAuthenticated: false })
      .pipe(
        tap(() => this.multimediasStore.setLoading(true)),
        map(items =>
          items.map(i => ({
            ...i,
            id: `${type}-${i.id}`,
            type: type,
            nombre: i.nombre || i.titulo
          }))
        ),
        tap(items => this.multimediasStore.set(items)),
        catchError(err => {
          console.error(err);
          return of();
        }),
        tap(() => this.multimediasStore.setLoading(false))
      )
      .subscribe();
  }
}
