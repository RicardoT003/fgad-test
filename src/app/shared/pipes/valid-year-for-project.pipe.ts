import { Pipe, PipeTransform, Sanitizer } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { prependBackendUrl } from "src/app/lib/helpers/files.helper";
import { Proyecto } from 'src/app/features/statistics/models/proyecto.model';

@Pipe({
    name: "validYearForProject"
})
export class ValidYearForProjectPipe implements PipeTransform {
    transform(yearToValidate: string, projectWithYears: Proyecto) {
        return !!projectWithYears.annios.find(a => a.valor === yearToValidate);
    }
}
