import { EntityState } from "@datorama/akita";
import { InicioImagen } from "../../domain/inicio-imagen.model";

export interface InicioImagenesState extends EntityState<InicioImagen> {}
