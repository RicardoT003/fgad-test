import { Injectable } from "@angular/core";
import { AppLayoutStore } from "../state/app-layout/store";

@Injectable({
  providedIn: "root"
})
export class AppLayoutService {
  constructor(private appLayoutStore: AppLayoutStore) {}

  showLandingLayout() {
    this.appLayoutStore.update({
      pushNavbar: false
    });
  }

  showCommonLayout() {
    this.appLayoutStore.update({
      pushNavbar: true
    });
  }

  setShowMobileNavbar(show) {
    this.appLayoutStore.update({
      showMobileNavbar: show
    });
  }
}
