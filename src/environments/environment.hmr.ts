import { environmentBase } from "./environment.base";

export const environment = {
  ...environmentBase,
  backendUrl: "http://134.209.160.88:1391",
};
