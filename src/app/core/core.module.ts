import { NgModule } from "@angular/core";
import { AppUtilsModule } from "./utils/app.utils.module";
import { AppStateModule } from "./state/app-state.module";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppServicesModule } from "./services/app-services.module";
import { AppCoreComponentsModule } from "./components/components.module";
import { NgProgressModule } from "ngx-progressbar";
import { NgProgressHttpModule } from "ngx-progressbar/http";
import { NgProgressRouterModule } from "ngx-progressbar/router";

@NgModule({
  declarations: [],
  imports: [
    AppUtilsModule,
    AppStateModule,
    AppServicesModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppCoreComponentsModule,
    NgProgressModule,
    NgProgressHttpModule,
    NgProgressRouterModule
  ],
  exports: [AppCoreComponentsModule, NgProgressModule]
})
export class CoreModule {}
