import { Component, OnInit, OnDestroy, Input, ViewChild, ElementRef } from "@angular/core";
import { Observable } from "rxjs";
import { Proyecto } from "../../models/proyecto.model";
import { ProyectosQuery } from "../../state/proyectos/query";
import { FormControl } from "@angular/forms";
import { ProyectosStore } from "../../state/proyectos/store";
import { tap, filter, delay } from "rxjs/operators";
import { takeUntilDestroy } from "src/app/lib/helpers/rxjs.helper";
import { AppConstantsQuery } from 'src/app/core/state/app-constants/query';
import { EventsUtil } from 'src/app/core/utils/events.util';

@Component({
  selector: "app-challenges-select-and-description",
  templateUrl: "./challenges-select-and-description.component.html",
  styleUrls: ["./challenges-select-and-description.component.scss"]
})
export class ChallengesSelectAndDescriptionComponent
  implements OnInit, OnDestroy {
  @Input() tipoProyecto: string;
  @Input() selectPlaceholder: string;
  @ViewChild("projectInfoSection", { static: false }) projectInfoSectionElm: ElementRef;
  proyectos$: Observable<Proyecto[]>;
  activeProyecto$: Observable<Proyecto>;
  proyectoFC = new FormControl(null);
  avancesUrl: string;

  constructor(
    private proyectosQuery: ProyectosQuery,
    private proyectosStore: ProyectosStore,
    private appConstantsQuery: AppConstantsQuery,
    private events: EventsUtil
  ) {
  }

  ngOnInit() {
    this.avancesUrl = this.appConstantsQuery.getByCode('CODIGO_URL_PAGINA_PROYECTOS_ANUALES_AVANCE').valor;
    this.proyectos$ = this.proyectosQuery.selectByType(this.tipoProyecto);
    this.activeProyecto$ = this.proyectosQuery.selectActive() as Observable<
      Proyecto
    >;
    (this.proyectosQuery.selectActive() as Observable<Proyecto>)
      .pipe(
        filter(proyecto => !!proyecto),
        tap(proyecto => this.proyectoFC.setValue(proyecto.id)),
        takeUntilDestroy(this)
      ).subscribe();
    this.proyectoFC.valueChanges
      .pipe(
        tap(proyectoId => this.proyectosStore.setActive(proyectoId)),
        takeUntilDestroy(this)
      )
      .subscribe();
    this.events.scrollIntoProject.pipe(
      takeUntilDestroy(this),
      delay(100),
      tap(() => this.projectInfoSectionElm.nativeElement.scrollIntoView())
    ).subscribe();
  }

  ngOnDestroy() {
    this.proyectosStore.setActive(null);
  }
}
