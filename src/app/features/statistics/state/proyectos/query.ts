import { QueryEntity, QueryConfig, Order } from "@datorama/akita";
import { ProyectosState } from "./state";
import { Injectable } from "@angular/core";
import { ProyectosStore } from "./store";

@Injectable({
  providedIn: "root"
})
@QueryConfig({
  sortBy: "orden",
  sortByOrder: Order.ASC
})
export class ProyectosQuery extends QueryEntity<ProyectosState> {

  selectByType = (type) => this.selectAll({ filterBy: e => e.tipo === type });

  constructor(protected store: ProyectosStore) {
    super(store);
  }
}
