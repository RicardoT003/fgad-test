import { Component, OnInit } from "@angular/core";
import { InicioImagen } from "../../domain/inicio-imagen.model";
import { Observable } from "rxjs";
import { InicioImagenesQuery } from "../../state/inicio-imagenes/query";
import { tap } from "rxjs/operators";
import { AppMenusService } from "src/app/core/services/app-menus.service";

@Component({
  selector: "app-principal",
  templateUrl: "./principal.component.html",
  styleUrls: ["./principal.component.scss"],
})
export class PrincipalComponent implements OnInit {
  config = null;
  slides$: Observable<InicioImagen[]>;
  swiperAutoplayDelay = 6000;

  constructor(
    private inicioImagenesQuery: InicioImagenesQuery,
    private appMenusService: AppMenusService
  ) {
    this.config = {
      pagination: { el: ".swiper-pagination", clickable: true },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      // autoplay: {
      //   delay: this.swiperAutoplayDelay,
      // },
    };
    this.slides$ = this.inicioImagenesQuery.inicioImagenes$;
    this.inicioImagenesQuery.loading$;
  }

  ngOnInit() {}
}
