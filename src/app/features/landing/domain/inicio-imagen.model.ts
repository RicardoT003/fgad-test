import { BaseModel } from "src/app/domain/models/base/base.model";
import { FileModel } from "src/app/domain/models/file.model";

export class InicioImagen extends BaseModel<InicioImagen> {
  contenido: string;
  contenidoImagen: FileModel;
  textoBoton: string;
  ordenSlider: number;
  activo: boolean;
  imagenFondo: FileModel;
  imagenFondoMobile: FileModel;
}
