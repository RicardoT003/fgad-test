import { Component, OnInit, Input } from '@angular/core';
import { FichaTecnica } from '../../models/ficha-tecnica.model';
import { MatDialog } from '@angular/material/dialog';
import { FichaTecnicaModalComponent } from '../ficha-tecnica-modal/ficha-tecnica-modal.component';

@Component({
  selector: 'app-ficha-tecnica-trigger',
  templateUrl: './ficha-tecnica-trigger.component.html',
  styleUrls: ['./ficha-tecnica-trigger.component.scss']
})
export class FichaTecnicaTriggerComponent implements OnInit {

  @Input() fichaTecnica: FichaTecnica;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openModal() {
    this.dialog.open(FichaTecnicaModalComponent, {
      data: {
        fichaTecnica: this.fichaTecnica
      }
    });
  }

}
