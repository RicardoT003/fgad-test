import { QueryEntity, QueryConfig, Order } from "@datorama/akita";
import { MultimediasState } from "./state";
import { Injectable } from "@angular/core";
import { MultimediaStore } from "./store";

@Injectable({
  providedIn: "root"
})
@QueryConfig({
  sortBy: "orden",
  sortByOrder: Order.ASC
})
export class MultimediasQuery extends QueryEntity<MultimediasState> {
  get comiteLoaded() {
    return this.getAll({ filterBy: e => e.type === "comite" }).length > 0;
  }
  get documentosClaveLoaded() {
    return (
      this.getAll({ filterBy: e => e.type === "documentoClave" }).length > 0
    );
  }
  comite$ = this.selectAll({
    filterBy: entity => entity.type === "comite"
  });
  documentosClave$ = this.selectAll({
    filterBy: entity => entity.type === "documentoClave"
  });
  constructor(protected store: MultimediaStore) {
    super(store);
  }
}
