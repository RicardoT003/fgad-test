import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PagesModule } from "./pages/pages.module";
import { AuthenticationComponent } from "./authentication.component";
import { LoginComponent } from "./pages/login/login.component";
import { LoginGuard } from "./guards/login.guard";

const routes: Routes = [
  {
    path: "",
    component: AuthenticationComponent,
    children: [
      {
        path: "",
        component: LoginComponent,
        canActivate: [LoginGuard],
      },
    ],
  },
];

@NgModule({
  providers: [LoginGuard],
  imports: [RouterModule.forChild(routes), PagesModule],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
