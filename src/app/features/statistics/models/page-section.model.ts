import { BaseModel } from 'src/app/domain/models/base/base.model';
import { FileModel } from 'src/app/domain/models/file.model';

export class PageSection extends BaseModel<PageSection> {
    titulo: string;
    contenido: string;
    tipoGrafico: 'imagen' | 'graficoSpreadsheet' | 'graficoSpreadsheetMultiple';
    graficoSpreadsheetNombre: string;
    graficoSpreadsheetUrl: string;
    imagen: FileModel;
    fuente: string;
    graficos: { url: string, nombre: string }[];
}