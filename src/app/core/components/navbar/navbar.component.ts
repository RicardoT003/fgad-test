import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { AppMenusQuery } from "../../state/app-menus/query";
import { Observable } from "rxjs";
import { Menu } from "src/app/domain/models/menu.model";
import { map } from "rxjs/operators";
import { AppLayoutQuery } from "../../state/app-layout/query";
import { AppSessionService } from "../../services/app-session.service";
import { AppSessionQuery } from "../../state/app-session/query";
import { AppLayoutStore } from "../../state/app-layout/store";
import { AppLayoutService } from "../../services/app-layout.service";

@Component({
  selector: "strategic-management-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavbarComponent implements OnInit {
  menus$: Observable<Menu[]>;

  constructor(
    public appMenusQuery: AppMenusQuery,
    public appLayoutQuery: AppLayoutQuery,
    public appLayoutStore: AppLayoutStore,
    public appLayoutService: AppLayoutService,
    public appSessionQuery: AppSessionQuery,
    public appSessionSvc: AppSessionService
  ) {
    this.menus$ = this.appMenusQuery.navigationMenus$;
  }

  ngOnInit() {}
}
