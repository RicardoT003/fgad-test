import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvancesTableComponent } from './avances-table.component';

describe('AvancesTableComponent', () => {
  let component: AvancesTableComponent;
  let fixture: ComponentFixture<AvancesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvancesTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvancesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
