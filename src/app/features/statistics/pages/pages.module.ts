import { NgModule } from "@angular/core";
import { ComponentsModule } from "../components/components.module";
import { StrategicInfoComponent } from './strategic-info/strategic-info.component';
import { AppSharedModule } from 'src/app/shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [StrategicInfoComponent],
  imports: [AppSharedModule, RouterModule, ComponentsModule],
  exports: [StrategicInfoComponent]
})
export class PagesModule { }
