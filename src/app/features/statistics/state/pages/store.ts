import { EntityStore, StoreConfig } from '@datorama/akita';
import { PagesState } from './state';
import { Injectable } from '@angular/core';

@StoreConfig({
    name: 'pages'
})
@Injectable({
    providedIn: 'root'
})
export class PagesStore extends EntityStore<PagesState> {
    constructor() {
        super();
    }
}