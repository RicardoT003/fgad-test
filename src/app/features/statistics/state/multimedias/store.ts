import { EntityStore, StoreConfig } from "@datorama/akita";
import { MultimediasState } from "./state";
import { Injectable } from "@angular/core";

@StoreConfig({
  name: "multimedia"
})
@Injectable({
  providedIn: "root"
})
export class MultimediaStore extends EntityStore<MultimediasState> {
  constructor() {
    super();
  }
}
