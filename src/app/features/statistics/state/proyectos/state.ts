import { EntityState } from "@datorama/akita";
import { Proyecto } from "../../models/proyecto.model";

export interface ProyectosState extends EntityState<Proyecto> {}
