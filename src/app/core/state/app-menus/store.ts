import { StoreConfig, EntityStore } from "@datorama/akita";
import { Injectable } from "@angular/core";
import { AppMenusState } from "./state";

@StoreConfig({
  name: "menus"
})
@Injectable({
  providedIn: "root"
})
export class AppMenusStore extends EntityStore<AppMenusState> {
  constructor() {
    super({
      ui: {
        activatedMenuCode: null,
        activatedSubmenuCode: null,
        activatedSubmenu2Code: null
      }
    });
  }
}
