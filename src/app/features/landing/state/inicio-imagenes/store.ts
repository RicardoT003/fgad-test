import { EntityStore, StoreConfig } from "@datorama/akita";
import { InicioImagenesState } from "./state";
import { Injectable } from "@angular/core";

@StoreConfig({
  name: "inicioImagenes"
})
@Injectable({
  providedIn: "root"
})
export class InicioImagenesStore extends EntityStore<InicioImagenesState> {
  constructor() {
    super();
  }
}
