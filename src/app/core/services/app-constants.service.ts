import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { tap, map, catchError } from "rxjs/operators";

import { AppConstantsStore } from "../state/app-constants/store";
import { ApiUtil } from "../utils/api.util";
import { Constant } from "src/app/domain/models/constant.model";

@Injectable({
  providedIn: "root"
})
export class AppConstantsService {
  constructor(
    private api: ApiUtil,
    private appConstantsStore: AppConstantsStore
  ) {}

  loadConstants(): Observable<boolean> {
    return this.api.get("/constantes", { isAuthenticated: false }).pipe(
      tap((response: Constant[]) => this.appConstantsStore.set(response)),
      map(() => true),
      catchError(err => {
        this.appConstantsStore.setError(err);
        return of(true);
      })
    );
  }
}
