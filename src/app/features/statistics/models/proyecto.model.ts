import { FileModel } from "src/app/domain/models/file.model";
import { BaseModel } from "src/app/domain/models/base/base.model";
import { Annio } from './annio.model';

export class Proyecto extends BaseModel<Proyecto> {
  tipo: string;
  codigo: string;
  nombre: string;
  description: string;
  responsable: string;
  impacto: string;
  complejidad: string;
  promedio: string;
  estado: string;
  espacioDeResolucion: string;
  resultados: FileModel;
  imagen: FileModel;
  orden: number;
  porcentajeAvance: number;
  annios: Annio[];
}
