import { EntityState } from "@datorama/akita";
import { Multimedia } from "../../models/multimedia.model";

export interface MultimediasState extends EntityState<Multimedia> {}
