import { AppInitializationService } from "./core/services/app-initialization.service";

export const appInitializer = (
  appInitializationService: AppInitializationService
) => {
  return () => appInitializationService.init();
};
