import { EntityState } from "@datorama/akita";
import { Constant } from "src/app/domain/models/constant.model";

export interface AppConstantsState extends EntityState<Constant> {}
