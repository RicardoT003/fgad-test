import { ApiUtil } from "src/app/core/utils/api.util";
import { AppSessionStore } from "src/app/core/state/app-session/store";
import { Observable, of, empty } from "rxjs";
import { catchError, tap, flatMap, map } from "rxjs/operators";
import { AppMenusService } from "src/app/core/services/app-menus.service";
import { Injectable } from "@angular/core";
import { AppToastService } from "src/app/core/services/app-toast.service";

@Injectable({
  providedIn: "root"
})
export class AuthenticationService {
  constructor(
    private api: ApiUtil,
    private appSessionStore: AppSessionStore,
    private appMenusService: AppMenusService,
    private appToast: AppToastService
  ) {
    this.appSessionStore.setError(null);
  }

  login(req: { email: string; password: string }): Observable<boolean> {
    return this.api
      .post(
        "/auth/local",
        {
          identifier: req.email,
          password: req.password
        },
        {
          isAuthenticated: false
        }
      )
      .pipe(
        tap(({ user, jwt: token }) =>
          this.appSessionStore.update({ user, token })
        ),
        tap(() => this.appSessionStore.setError(null)),
        tap(() => this.appToast.success("Inicio de sesión exitoso")),
        flatMap(() => this.appMenusService.loadMenus()),
        map(() => true),
        catchError(err => {
          this.appSessionStore.setError(err.error);
          return of(false);
        })
      );
  }
  changePassword({ currentPassword, newPassword }) {
    return this.api.post('/auth/change-password', {
      currentPassword,
      newPassword
    }).pipe(
      tap(() => this.appToast.success("Contraseña cambiada con éxito")),
      map(() => true),
      catchError(err => {
        return of(err.error.message);
      })
    );
  }
}
