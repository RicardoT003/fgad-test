import { StoreConfig, EntityStore } from "@datorama/akita";
import { AppConstantsState } from "./state";
import { Injectable } from "@angular/core";

@StoreConfig({
  name: "constants"
})
@Injectable({
  providedIn: "root"
})
export class AppConstantsStore extends EntityStore<AppConstantsState> {
  constructor() {
    super();
  }
}
