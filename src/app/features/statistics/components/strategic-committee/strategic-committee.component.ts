import { Component, OnInit } from "@angular/core";
import { MultimediasQuery } from "../../state/multimedias/query";
import { Observable } from "rxjs";
import { Multimedia } from "../../models/multimedia.model";
import { MultimediasService } from "../../services/multimedias.service";

@Component({
  selector: "app-strategic-committee",
  templateUrl: "./strategic-committee.component.html",
  styleUrls: ["./strategic-committee.component.scss"]
})
export class StrategicCommitteeComponent implements OnInit {
  comite$: Observable<Multimedia[]>;

  constructor(
    private multimediasQuery: MultimediasQuery,
    private multimediasSvc: MultimediasService
  ) {
    this.comite$ = this.multimediasQuery.comite$;
    this.multimediasSvc.loadMultimedias("comite");
  }

  ngOnInit() {}
}
