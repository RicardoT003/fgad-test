import { CanActivate, ActivatedRouteSnapshot, CanActivateChild } from "@angular/router";
import { Injectable } from "@angular/core";
import { AppMenusService } from "src/app/core/services/app-menus.service";

@Injectable()
export class SaveMenuStateGuard implements CanActivate {
  constructor(private appMenuSvc: AppMenusService) { }
  canActivate(route: ActivatedRouteSnapshot): boolean {
    const { menuCode, submenuCode, submenu2Code } = route.params;
    this.appMenuSvc.saveMenuUiState(menuCode, submenuCode, submenu2Code);
    return true;
  }
}
