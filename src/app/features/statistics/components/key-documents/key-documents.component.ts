import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { Multimedia } from "../../models/multimedia.model";
import { MultimediasQuery } from "../../state/multimedias/query";
import { MultimediasService } from "../../services/multimedias.service";
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: "app-key-documents",
  templateUrl: "./key-documents.component.html",
  styleUrls: ["./key-documents.component.scss"]
})
export class KeyDocumentsComponent implements OnInit {
  documents$: Observable<Multimedia[]>;

  constructor(
    private multimediasQuery: MultimediasQuery,
    private multimediasSvc: MultimediasService,
    public sanitizer: DomSanitizer
  ) {
    this.documents$ = this.multimediasQuery.documentosClave$;
    this.multimediasSvc.loadMultimedias("documentoClave");
  }

  ngOnInit() { }
}
