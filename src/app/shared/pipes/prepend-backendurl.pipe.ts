import { Pipe, PipeTransform } from "@angular/core";
import { prependBackendUrl } from "src/app/lib/helpers/files.helper";

@Pipe({
  name: "prependBackendurl"
})
export class PrependBackendurlPipe implements PipeTransform {
  transform(value: any) {
    return prependBackendUrl(value);
  }
}
