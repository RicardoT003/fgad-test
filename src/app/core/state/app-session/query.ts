import { AppSessionState } from "./state";
import { AppSessionStore } from "./store";
import { Query } from "@datorama/akita";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class AppSessionQuery extends Query<AppSessionState> {
  get token() {
    return this.getValue().token;
  }
  user$ = this.select(state => state.user);
  isLogged$ = this.select(state => !!state.user);

  constructor(protected store: AppSessionStore) {
    super(store);
  }
}
