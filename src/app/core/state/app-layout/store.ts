import { Store, StoreConfig } from "@datorama/akita";
import { AppLayoutState } from "./state";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
@StoreConfig({
  name: "layout"
})
export class AppLayoutStore extends Store<AppLayoutState> {
  constructor() {
    super({
      showNavbar: true,
      pushNavbar: true,
      headerMode: "common"
    });
  }
}
