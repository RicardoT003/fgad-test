import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "auth",
    loadChildren: () =>
      import("./features/authentication/authentication.module").then(
        m => m.AuthenticationModule
      )
  },
  {
    path: "estadisticas",
    loadChildren: () =>
      import("./features/statistics/statistics.module").then(
        m => m.StatisticsModule
      )
  },
  {
    path: "",
    loadChildren: () =>
      import("./features/landing/landing.module").then(m => m.LandingModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
