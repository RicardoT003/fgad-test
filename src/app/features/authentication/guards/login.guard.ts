import { CanActivate, Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { AppToastService } from "src/app/core/services/app-toast.service";
import { AppSessionQuery } from "src/app/core/state/app-session/query";
import { first, map, tap } from "rxjs/operators";
import { Observable } from "rxjs";

@Injectable()
export class LoginGuard implements CanActivate {
  constructor(
    private appToast: AppToastService,
    private appSessionQuery: AppSessionQuery,
    private router: Router
  ) {}
  canActivate(): Observable<boolean> {
    return this.appSessionQuery.isLogged$.pipe(
      first(),
      map((logged) => !logged),
      tap(
        (canActivate) =>
          !canActivate && this.appToast.info("Ya has iniciado sesión")
      ),
      tap((canActivate) => !canActivate && this.router.navigateByUrl("/"))
    );
  }
}
