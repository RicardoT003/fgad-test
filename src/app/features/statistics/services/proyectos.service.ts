import { Injectable } from "@angular/core";
import { ProyectosStore } from "../state/proyectos/store";
import { ProyectosQuery } from "../state/proyectos/query";
import { Observable, of } from "rxjs";
import { ApiUtil } from "src/app/core/utils/api.util";
import { map, flatMap, tap, catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ProyectosService {
  constructor(
    private proyectosStore: ProyectosStore,
    private proyectosQuery: ProyectosQuery,
    private api: ApiUtil
  ) {}

  loadProyectos(): Observable<boolean> {
    const getProyectos$ = this.api.get("/proyectos").pipe(
      tap(response => this.proyectosStore.set(response)),
      map(() => true),
      catchError(err => {
        console.error(err);
        this.proyectosStore.setError(err);
        return of(false);
      })
    );
    return this.proyectosQuery
      .selectLoading()
      .pipe(flatMap(loading => (loading ? getProyectos$ : of(true))));
  }
}
