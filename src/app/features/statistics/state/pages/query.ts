import { QueryEntity } from '@datorama/akita';
import { PagesState } from './state';
import { Injectable } from '@angular/core';
import { PagesStore } from './store';

@Injectable({
    providedIn: 'root'
})
export class PagesQuery extends QueryEntity<PagesState> {
    constructor(protected pagesStore: PagesStore) {
        super(pagesStore);
    }
}