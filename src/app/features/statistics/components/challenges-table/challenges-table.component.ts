import { Component, OnInit } from "@angular/core";
import { ProyectosQuery } from "../../state/proyectos/query";
import { Observable } from "rxjs";
import { Proyecto } from "../../models/proyecto.model";
import { ProyectosStore } from "../../state/proyectos/store";
import { EventsUtil } from "src/app/core/utils/events.util";

@Component({
  selector: "app-challenges-table",
  templateUrl: "./challenges-table.component.html",
  styleUrls: ["./challenges-table.component.scss"]
})
export class ChallengesTableComponent implements OnInit {
  proyectos$: Observable<Proyecto[]>;

  constructor(
    private proyectosQuery: ProyectosQuery,
    public proyectosStore: ProyectosStore,
    private events: EventsUtil
  ) {
    this.proyectos$ = this.proyectosQuery.selectByType("desafio");
  }

  ngOnInit() {}

  scrollToProyect() {
    this.events.scrollIntoProject.emit();
  }
}
