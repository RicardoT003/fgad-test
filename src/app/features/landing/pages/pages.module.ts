import { NgModule } from "@angular/core";
import { ComponentsModule } from "../components/components.module";
import { NgxUsefulSwiperModule } from "ngx-useful-swiper";
import { AppSharedModule } from "src/app/shared/shared.module";
import { PrincipalComponent } from './principal/principal.component';

@NgModule({
  declarations: [PrincipalComponent],
  imports: [ComponentsModule, AppSharedModule, NgxUsefulSwiperModule],
  exports: [PrincipalComponent]
})
export class PagesModule {}
