import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatDialogModule } from '@angular/material/dialog';

import { AppSharedComponentsModule } from "./components/app-components.module";
import { AppDirectivesModule } from "./directives/app-directives.module";
import { AppPipesModule } from "./pipes/app-pipes.module";

@NgModule({
  declarations: [],
  imports: [],
  exports: [
    CommonModule,
    AppSharedComponentsModule,
    AppDirectivesModule,
    AppPipesModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule
  ]
})
export class AppSharedModule { }
