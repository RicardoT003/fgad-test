import { Pipe, PipeTransform } from "@angular/core";
import * as showdown from "showdown";
import { DomSanitizer } from "@angular/platform-browser";
const converter = new showdown.Converter();

@Pipe({
  name: "htmlFromMarkup"
})
export class HtmlFromMarkupPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(markup = "") {
    return this.sanitizer.bypassSecurityTrustHtml(converter.makeHtml(markup));
  }
}
