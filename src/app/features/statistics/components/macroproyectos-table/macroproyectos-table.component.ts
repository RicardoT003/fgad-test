import { Component, OnInit } from '@angular/core';
import { AnniosQuery } from '../../state/annios/query';
import { Observable } from 'rxjs';
import { Annio } from '../../models/annio.model';
import { ProyectosQuery } from '../../state/proyectos/query';
import { Proyecto } from '../../models/proyecto.model';
import { ProyectosStore } from '../../state/proyectos/store';
import { EventsUtil } from 'src/app/core/utils/events.util';

@Component({
  selector: 'app-macroproyectos-table',
  templateUrl: './macroproyectos-table.component.html',
  styleUrls: ['./macroproyectos-table.component.scss']
})
export class MacroproyectosTableComponent implements OnInit {

  annios$: Observable<Annio[]>;
  proyectos$: Observable<Proyecto[]>;

  constructor(private anniosQuery: AnniosQuery, private proyectosQuery: ProyectosQuery, public proyectosStore: ProyectosStore,
    public events: EventsUtil) {
    this.annios$ = this.anniosQuery.selectAll();
    this.proyectos$ = this.proyectosQuery.selectByType('estrategico:macroproyecto');
  }

  ngOnInit() {
  }

}
