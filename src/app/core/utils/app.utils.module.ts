import { NgModule } from "@angular/core";
import { ApiUtil } from "./api.util";
import { HttpClientModule } from "@angular/common/http";
import { EventsUtil } from './events.util';

@NgModule({
  imports: [HttpClientModule],
  providers: [ApiUtil, EventsUtil]
})
export class AppUtilsModule { }
