import { BaseModel } from "./base/base.model";

export class User extends BaseModel<User> {
  email: string;
  password: string;
}
