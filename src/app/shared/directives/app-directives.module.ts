import { NgModule } from "@angular/core";
import { MenuCodeActiveDirective } from './menu-code-active.directive';

@NgModule({
  declarations: [MenuCodeActiveDirective],
  imports: [],
  exports: [MenuCodeActiveDirective]
})
export class AppDirectivesModule { }
