import { AppConstantsService } from "./app-constants.service";
import { forkJoin } from "rxjs";
import { Injectable } from "@angular/core";
import { AppMenusService } from "./app-menus.service";

@Injectable({
  providedIn: "root"
})
export class AppInitializationService {
  constructor(
    private appConstantsService: AppConstantsService,
    private appMenusSvc: AppMenusService
  ) {}

  init(): Promise<any> {
    return forkJoin(
      this.appConstantsService.loadConstants(),
      this.appMenusSvc.loadMenus()
    ).toPromise();
  }
}
