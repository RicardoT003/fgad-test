import { NgModule } from "@angular/core";

import { StatisticsRoutingModule } from "./statistics-routing.module";
import { StatisticsComponent } from "./statistics.component";
import { ComponentsModule } from "./components/components.module";
import { PagesStore } from "./state/pages/store";
import { PagesQuery } from "./state/pages/query";
import { PagesService } from "./services/pages.service";
import { MultimediaStore } from "./state/multimedias/store";
import { MultimediasQuery } from "./state/multimedias/query";
import { MultimediasService } from "./services/multimedias.service";
import { ProyectosStore } from "./state/proyectos/store";
import { ProyectosQuery } from "./state/proyectos/query";
import { ProyectosService } from "./services/proyectos.service";
import { AnniosStore } from './state/annios/store';
import { AnniosQuery } from './state/annios/query';
import { AnniosService } from './services/annios.service';

@NgModule({
  declarations: [StatisticsComponent],
  imports: [StatisticsRoutingModule, ComponentsModule],
  providers: [
    PagesStore,
    PagesQuery,
    PagesService,
    MultimediaStore,
    MultimediasQuery,
    MultimediasService,
    ProyectosStore,
    ProyectosQuery,
    ProyectosService,
    AnniosStore,
    AnniosQuery,
    AnniosService
  ]
})
export class StatisticsModule { }
