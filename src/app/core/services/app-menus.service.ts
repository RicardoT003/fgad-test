import { Injectable } from "@angular/core";
import { ApiUtil } from "../utils/api.util";
import { AppMenusStore } from "../state/app-menus/store";
import { Observable, of } from "rxjs";
import { tap, catchError, first, flatMap } from "rxjs/operators";
import { AppSessionQuery } from "../state/app-session/query";

@Injectable({
  providedIn: "root",
})
export class AppMenusService {
  constructor(
    private api: ApiUtil,
    private appMenusStore: AppMenusStore,
    private appSessionQuery: AppSessionQuery
  ) {}

  loadMenus(): Observable<boolean> {
    return this.appSessionQuery.isLogged$.pipe(
      first(),
      flatMap((logged) => this.api.get("/menus", { isAuthenticated: logged })),
      tap((resp) => this.appMenusStore.set(resp)),
      catchError((err) => of(this.appMenusStore.setError(err)))
    );
  }

  loadMenusOpt() {
    // return this.appSessionQuery.isLogged$.pipe(
    //   first(),
    //   flatMap(logged => this.api.get("/menus", { isAuthenticated: logged })),
    //   tap(resp => this.appMenusStore.set(resp)),
    //   catchError(err => of(this.appMenusStore.setError(err)))
    // );
    return this.api.get("/menus", { isAuthenticated: false }).pipe(
      tap((vv) => console.log("vvv", vv)),
      tap((resp) => this.appMenusStore.set(resp))
    );
  }

  saveMenuUiState(
    menuCode: string,
    submenuCode: string,
    submenu2Code?: string
  ) {
    this.appMenusStore.update({
      ui: {
        activatedMenuCode: menuCode,
        activatedSubmenuCode: submenuCode,
        activatedSubmenu2Code: submenu2Code,
      },
    });
  }
}
