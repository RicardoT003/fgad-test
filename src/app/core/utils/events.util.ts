import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class EventsUtil {

    scrollIntoProject = new EventEmitter();

}