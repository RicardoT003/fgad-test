import { Component, OnInit } from "@angular/core";
import { PagesQuery } from "../../state/pages/query";
import { Observable, combineLatest } from "rxjs";
import { Page } from "../../models/page.model";
import { ActivatedRoute } from "@angular/router";
import { takeUntilDestroy } from "src/app/lib/helpers/rxjs.helper";
import { tap, filter, map } from "rxjs/operators";
import { AppConstantsQuery } from "src/app/core/state/app-constants/query";
import { DomSanitizer } from "@angular/platform-browser";
import { Menu } from "src/app/domain/models/menu.model";
import { AppMenusQuery } from "src/app/core/state/app-menus/query";

@Component({
  selector: "app-strategic-info",
  templateUrl: "./strategic-info.component.html",
  styleUrls: ["./strategic-info.component.scss"]
})
export class StrategicInfoComponent implements OnInit {
  page$: Observable<Page>;
  showComittee = false;
  showDocumentosClave = false;
  showMapaEstrategicoMensaje = false;
  isPortafolioDesafios: boolean;
  isMacroproyectos: boolean;
  isProyectosAnualesPortafolio: boolean;
  isProyectosAnualesAvance: boolean;
  mensajeMapaEstrategico = "";
  titulosSeccionFichaTecnica = [];
  titulosSeccionAvancesProyectos = [];
  activeSpreadsheetNombre = "";
  activatedSubmenu$: Observable<Menu>;
  proyectosAnualesAvanceUrl: string;

  constructor(
    private pagesQuery: PagesQuery,
    private route: ActivatedRoute,
    private constantsQuery: AppConstantsQuery,
    public sanitizer: DomSanitizer,
    private appMenusQuery: AppMenusQuery
  ) {
    const comitteeUrl = this.constantsQuery.getByCode(
      "CODIGO_URL_PAGINA_COMITE_ESTRATEGICO"
    ).valor;
    const documentosClaveUrl = this.constantsQuery.getByCode(
      "CODIGO_URL_PAGINA_DOCUMENTOS_CLAVE"
    ).valor;
    const mapaEstrategicoUrl = this.constantsQuery.getByCode(
      "CODIGO_URL_PAGINA_MAPA_ESTRATEGICO"
    ).valor;
    const portafolioDesafiosUrl = this.constantsQuery.getByCode(
      "CODIGO_URL_PAGINA_PORTAFOLIO_DESAFIOS"
    ).valor;
    const macroproyectosUrl = this.constantsQuery.getByCode(
      "CODIGO_URL_PAGINA_MACROPROYECTOS"
    ).valor;
    const proyectosAnualesPortafolioUrl = this.constantsQuery.getByCode(
      "CODIGO_URL_PAGINA_PROYECTOS_ANUALES_PORTAFOLIO"
    ).valor;
    const proyectosAnualesUrlsRegExp = new RegExp(
      this.constantsQuery.getByCode("PROYECTOS_ANUALES_URLS_REGEXP").valor
    );
    this.proyectosAnualesAvanceUrl = this.constantsQuery.getByCode(
      "CODIGO_URL_PAGINA_PROYECTOS_ANUALES_AVANCE"
    ).valor;
    this.titulosSeccionFichaTecnica = this.constantsQuery
      .getByCode("FICHA_TECNICA_SECCION_TITULOS")
      .valor.split("|");
    this.titulosSeccionAvancesProyectos = this.constantsQuery
      .getByCode("AVANCES_PROYECTOS_SECCION_TITULOS")
      .valor.split("|");
    this.mensajeMapaEstrategico = this.constantsQuery.getByCode(
      "MAPA_ESTRATEGICO_MENSAJE"
    ).valor;
    this.page$ = this.pagesQuery.selectActive() as Observable<Page>;
    this.activatedSubmenu$ = combineLatest(
      this.appMenusQuery.activatedSubMenuCode$,
      this.appMenusQuery.activatedSubMenu2Code$
    ).pipe(
      filter(([_, submenu2Code]) => !!submenu2Code),
      map(([submenuCode]) => this.appMenusQuery.getByCode(submenuCode))
    );
    this.route.params
      .pipe(
        takeUntilDestroy(this),
        tap(() => {
          const { pathname } = window.location;
          this.showComittee = comitteeUrl === pathname;
          this.showDocumentosClave = documentosClaveUrl === pathname;
          this.showMapaEstrategicoMensaje = mapaEstrategicoUrl === pathname;
          this.isPortafolioDesafios = portafolioDesafiosUrl === pathname;
          this.isMacroproyectos = macroproyectosUrl === pathname;
          this.isProyectosAnualesPortafolio = proyectosAnualesUrlsRegExp.test(
            pathname
          );
          this.isProyectosAnualesAvance = proyectosAnualesUrlsRegExp.test(
            pathname
          );
        })
      )
      .subscribe();
    this.page$
      .pipe(
        takeUntilDestroy(this),
        filter(page => !!page),
        tap((page: any) => {
          if (page.secciones[0].tipoGrafico === "graficoSpreadsheetMultiple") {
            this.activeSpreadsheetNombre = page.secciones[0].graficos[0].nombre;
          } else {
            this.activeSpreadsheetNombre = "";
          }
        })
      )
      .subscribe();
  }

  ngOnInit() {}

  mustContentSectionShowInTop(sectionIndex: number) {
    return (
      sectionIndex === 0 &&
      (this.isProyectosAnualesPortafolio || this.isProyectosAnualesAvance)
    );
  }
}
