import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { LandingComponent } from "./landing.component";
import { PagesModule } from "./pages/pages.module";
import { PrincipalComponent } from "./pages/principal/principal.component";

const routes: Routes = [
  {
    path: "",
    component: LandingComponent,
    children: [
      {
        path: "",
        component: PrincipalComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), PagesModule],
  exports: [RouterModule]
})
export class LandingRoutingModule {}
