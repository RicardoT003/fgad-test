export class BaseModel<T> {
  id: any;
  constructor(partial?: Partial<T>) {
    Object.assign(this, partial);
  }
}
