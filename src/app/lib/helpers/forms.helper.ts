import {
  FormControl,
  AbstractControl,
  FormGroup,
  FormArray
} from "@angular/forms";
import { Validators as NgValidators } from "@angular/forms";

const isInvalidControl = (formControl: FormControl) => {
  return formControl.invalid && formControl.touched;
};

const markAllChildrenAsTouchedForFG = (fg: FormGroup) => {
  Object.keys(fg.controls)
    .map(key => fg.get(key))
    .forEach(control => {
      markAllChildrenAsTouched(control);
    });
};

const markChildrenAsTouchedForFA = (fa: FormArray) => {
  fa.controls.forEach(control => {
    markAllChildrenAsTouched(control);
  });
};

export const markAllChildrenAsTouched = (control: AbstractControl) => {
  if (control instanceof FormGroup) {
    markAllChildrenAsTouchedForFG(control);
  } else if (control instanceof FormArray) {
    markChildrenAsTouchedForFA(control);
  } else {
    control.markAsTouched();
  }
};

export const AppValidators = {
  email: () => {
    const emailValidatorFn = NgValidators.pattern(/.+@.+\..+/);
    const requiredValidatorFn = NgValidators.required;
    return (c: AbstractControl) => {
      return requiredValidatorFn(c) || emailValidatorFn(c);
    };
  },
  equalsToControl: (controlName: string) => {
    return (c: AbstractControl) => {
      if (!c.parent) return null;
      if (c.parent.get(controlName).value !== c.value)
        return { equalsToControl: true };
      return null;
    };
  },
  equalsTo: (value: any) => {
    return (c: AbstractControl) => {
      if (c.value !== value) return { equalsTo: true };
      return null;
    };
  }
};

export const AppFormBuilder = {
  commonControl: (
    defaultValue = null,
    validators = [NgValidators.required]
  ) => {
    return [defaultValue, validators];
  }
};
