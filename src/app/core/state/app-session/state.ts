import { User } from "src/app/domain/models/user.model";

export interface AppSessionState {
  token: string;
  user: User;
}
