import { BaseModel } from 'src/app/domain/models/base/base.model';

export class Annio extends BaseModel<Annio> {
    valor: string;
}
