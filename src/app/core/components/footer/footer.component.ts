import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input
} from "@angular/core";
import { Constant } from "src/app/domain/models/constant.model";

export interface FooterContactInfo {
  email: Constant;
  phone: Constant;
}

@Component({
  selector: "strategic-management-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent implements OnInit {
  @Input() contactInfo: FooterContactInfo;
  constructor() {}

  ngOnInit() {}
}
