import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { LandingRoutingModule } from "./landing-routing.module";
import { LandingComponent } from "./landing.component";
import { InicioImagenesService } from "./services/inicio-imagenes.service";
import { InicioImagenesStore } from "./state/inicio-imagenes/store";
import { InicioImagenesQuery } from "./state/inicio-imagenes/query";

@NgModule({
  providers: [InicioImagenesStore, InicioImagenesQuery, InicioImagenesService],
  declarations: [LandingComponent],
  imports: [CommonModule, LandingRoutingModule]
})
export class LandingModule {
  constructor(private inicioImagenesService: InicioImagenesService) {
    this.inicioImagenesService.loadInicioImagenes().subscribe();
  }
}
