import { environmentBase } from "./environment.base";

export const environment = {
  ...environmentBase,
  hmr: false,
  production: true,
  backendUrl: "http://134.209.160.88:1391",
};
