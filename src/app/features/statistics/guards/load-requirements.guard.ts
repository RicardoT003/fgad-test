import { CanActivate } from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable, forkJoin } from "rxjs";
import { ProyectosService } from "../services/proyectos.service";
import { first, map, tap } from "rxjs/operators";
import { AnniosService } from '../services/annios.service';

@Injectable()
export class LoadRequirementsGuard implements CanActivate {
  constructor(private proyectosService: ProyectosService, private annioService: AnniosService) { }

  canActivate(): Observable<boolean> {
    return forkJoin(
      this.proyectosService.loadProyectos().pipe(first()),
      this.annioService.loadAnnios().pipe(first())
    ).pipe(
      map(() => true)
    );
  }
}
