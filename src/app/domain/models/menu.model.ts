import { BaseModel } from "./base/base.model";
import { Page } from 'src/app/features/statistics/models/page.model';

export class Menu extends BaseModel<Menu> {
  codigo: string;
  texto: string;
  textoMenu: string;
  requiereAutenticacion: boolean;
  mostrarEnNavbar: boolean;
  subMenus: Menu[];
  menuPadre: Menu;
  pagina: Page;
  orden: number;
}
