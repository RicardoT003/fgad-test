import { Directive, Input, ElementRef, OnInit } from '@angular/core';
import { AppMenusService } from 'src/app/core/services/app-menus.service';
import { AppMenusQuery } from 'src/app/core/state/app-menus/query';
import { Observable } from 'rxjs';
import { takeUntilDestroy } from 'src/app/lib/helpers/rxjs.helper';
import { tap } from 'rxjs/operators';

@Directive({
    selector: '[menuCode]'
})
export class MenuCodeActiveDirective implements OnInit {

    @Input() menuCode: string;
    @Input() menuCodeActive: string;
    @Input() menuCodeType: 'menu' | 'submenu' | 'submenu2';

    constructor(private elementRef: ElementRef, private menusQuerySvc: AppMenusQuery) {

    }

    ngOnInit() {
        let activeMenuCode$: Observable<string> = null;
        switch (this.menuCodeType) {
            case 'menu':
                activeMenuCode$ = this.menusQuerySvc.activatedMenuCode$;
                break;
            case 'submenu':
                activeMenuCode$ = this.menusQuerySvc.activatedSubMenuCode$;
                break;
            case 'submenu2':
                activeMenuCode$ = this.menusQuerySvc.activatedSubMenu2Code$;
                break;
        }
        activeMenuCode$.pipe(
            takeUntilDestroy(this),
            tap((menuCode: any) => {
                if (menuCode === this.menuCode) {
                    this.elementRef.nativeElement.className += ' active';
                } else {
                    this.elementRef.nativeElement.className = this.elementRef.nativeElement.className.replace('active', '');
                }
            })
        ).subscribe();
    }
}