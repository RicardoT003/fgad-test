import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  AppValidators,
  markAllChildrenAsTouched
} from 'src/app/lib/helpers/forms.helper';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { AppConstantsQuery } from 'src/app/core/state/app-constants/query';
import { AppSessionQuery } from 'src/app/core/state/app-session/query';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  contactEmail: string;
  loginFG: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authSvc: AuthenticationService,
    private router: Router,
    public constantsQuery: AppConstantsQuery,
    public sessionQuery: AppSessionQuery
  ) {
    this.loginFG = this.fb.group({
      email: [null, [AppValidators.email]],
      password: [null, [Validators.required]]
    });
    this.contactEmail = (this.constantsQuery.getByCode('EMAIL_REGISTRO_RECUPERAR_PASSWORD') || {} as any).valor;
  }

  ngOnInit() { }

  login() {
    markAllChildrenAsTouched(this.loginFG);
    if (this.loginFG.invalid) {
      return;
    }
    const { email, password } = this.loginFG.value;
    this.authSvc
      .login({ email, password })
      .pipe(tap(logged => logged && this.router.navigateByUrl('/')))
      .subscribe();
  }
}
