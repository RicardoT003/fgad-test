import { EntityState } from '@datorama/akita';
import { Page } from '../../models/page.model';

export interface PagesState extends EntityState<Page> {

}