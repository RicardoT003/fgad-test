import { Injectable } from "@angular/core";
import { QueryEntity } from "@datorama/akita";
import { AppConstantsStore } from "./store";
import { AppConstantsState } from "./state";
import { Constant } from "src/app/domain/models/constant.model";

@Injectable({
  providedIn: "root"
})
export class AppConstantsQuery extends QueryEntity<AppConstantsState> {
  constructor(protected store: AppConstantsStore) {
    super(store);
  }

  getByCode(code: string): Constant {
    return this.getAll({
      filterBy: entity => entity.codigo === code
    })[0];
  }
}
