import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

import { ProyectosQuery } from '../../state/proyectos/query';
import { Proyecto } from '../../models/proyecto.model';

@Component({
  selector: 'app-avances-table',
  templateUrl: './avances-table.component.html',
  styleUrls: ['./avances-table.component.scss']
})
export class AvancesTableComponent implements OnInit {

  proyectos$: Observable<Proyecto[]>
  selectedProjectId$: Observable<string>;
  avanceGuides = [25, 50, 75, 100];

  constructor(public proyectosQuery: ProyectosQuery, public route: ActivatedRoute) {
    this.proyectos$ = this.proyectosQuery.selectByType('estrategico:anual');
    this.selectedProjectId$ = this.route.queryParams.pipe(map(q => q.pId));
  }

  ngOnInit() {
  }

}
