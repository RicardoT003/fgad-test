import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MacroproyectosTableComponent } from './macroproyectos-table.component';

describe('MacroproyectosTableComponent', () => {
  let component: MacroproyectosTableComponent;
  let fixture: ComponentFixture<MacroproyectosTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MacroproyectosTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MacroproyectosTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
