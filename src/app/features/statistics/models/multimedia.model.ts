import { BaseModel } from "src/app/domain/models/base/base.model";
import { FileModel } from "src/app/domain/models/file.model";

export class Multimedia extends BaseModel<Multimedia> {
  nombre: string;
  descripcion: string;
  foto: FileModel;
  type: "comite" | "documentoClave";
  documento: FileModel;
}
