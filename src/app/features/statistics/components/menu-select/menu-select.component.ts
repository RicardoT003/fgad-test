import { Component, OnInit } from "@angular/core";
import { AppMenusQuery } from "src/app/core/state/app-menus/query";
import { tap, map, delay } from "rxjs/operators";
import { AppConstantsQuery } from "src/app/core/state/app-constants/query";
import { FormControl } from "@angular/forms";
import { Observable, combineLatest } from "rxjs";
import { Menu } from "src/app/domain/models/menu.model";
import { takeUntilDestroy } from "src/app/lib/helpers/rxjs.helper";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-menu-select",
  templateUrl: "./menu-select.component.html",
  styleUrls: ["./menu-select.component.scss"]
})
export class MenuSelectComponent implements OnInit {
  show$: Observable<boolean>;
  subTreeMenu$: Observable<Menu[]>;
  selectedSubmenu2CodeFC = new FormControl(null);

  constructor(
    private appMenuQuery: AppMenusQuery,
    private appConstantsQuery: AppConstantsQuery,
    private router: Router,
    private route: ActivatedRoute
  ) {
    const showInMenuCodes = this.appConstantsQuery
      .getByCode("MENU_SELECT_MOSTRAR_CODIGOS_MENU")
      .valor.split(";");
    combineLatest(
      this.appMenuQuery.activatedSubMenuCode$,
      this.appMenuQuery.activatedSubMenu2Code$
    )
      .pipe(
        takeUntilDestroy(this),
        tap(([activatedSubmenuCode, activatedSubmenu2Code]) => {
          const path = `${activatedSubmenuCode}${
            activatedSubmenu2Code ? `/${activatedSubmenu2Code}` : ""
            }`;
          this.selectedSubmenu2CodeFC.setValue(path);
        })
      )
      .subscribe();
    this.show$ = this.appMenuQuery.activatedMenuCode$.pipe(
      map(menuCode => {
        return showInMenuCodes.includes(menuCode);
      })
    );
    this.subTreeMenu$ = combineLatest(
      this.appMenuQuery.activatedMenuCode$,
      this.appMenuQuery.treeMenu$
    ).pipe(
      map(([activatedMenu, treeMenu]) => {
        return treeMenu.find(m => m.codigo === activatedMenu).subMenus;
      })
    );
    this.selectedSubmenu2CodeFC.valueChanges
      .pipe(
        takeUntilDestroy(this),
        delay(200),
        tap(selectedPath => {
          this.router.navigate(
            [`/estadisticas/${this.appMenuQuery.activeMenuCode}/${selectedPath}`],
            {
              queryParams: this.route.snapshot.queryParams
            }
          );
        })
      )
      .subscribe();
  }

  ngOnInit() { }
}
