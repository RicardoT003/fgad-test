import { EntityState } from "@datorama/akita";
import { Menu } from "src/app/domain/models/menu.model";

export interface AppMenusState extends EntityState<Menu> {
  ui: {
    activatedMenuCode: string;
    activatedSubmenuCode: string;
    activatedSubmenu2Code: string;
  };
}
