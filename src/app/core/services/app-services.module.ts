import { NgModule } from "@angular/core";
import { AppSessionService } from "./app-session.service";
import { AppConstantsService } from "./app-constants.service";
import { AppInitializationService } from "./app-initialization.service";
import { AppMenusService } from "./app-menus.service";
import { AppLayoutService } from "./app-layout.service";
import { AppToastService } from "./app-toast.service";

@NgModule({
  providers: [
    AppSessionService,
    AppConstantsService,
    AppInitializationService,
    AppMenusService,
    AppLayoutService,
    AppToastService
  ]
})
export class AppServicesModule {}
