import { NgModule } from "@angular/core";
import { AppSessionStore } from "./app-session/store";
import { AppSessionQuery } from "./app-session/query";
import { AppConstantsQuery } from "./app-constants/query";
import { AppConstantsStore } from "./app-constants/store";
import { AppMenusStore } from "./app-menus/store";
import { AppMenusQuery } from "./app-menus/query";
import { AppLayoutStore } from "./app-layout/store";
import { AppLayoutQuery } from "./app-layout/query";

@NgModule({
  providers: [
    AppSessionStore,
    AppSessionQuery,
    AppConstantsQuery,
    AppConstantsStore,
    AppMenusStore,
    AppMenusQuery,
    AppLayoutStore,
    AppLayoutQuery
  ]
})
export class AppStateModule {}
