import { BaseModel } from 'src/app/domain/models/base/base.model';
import { PageSection } from './page-section.model';
import { FichaTecnica } from './ficha-tecnica.model';

export class Page extends BaseModel<Page> {
    codigoUrl: string;
    secciones: PageSection[];
    fichastenica: FichaTecnica;
    avancesproyectos: { descripcion: string, proyecto: string }[];
}