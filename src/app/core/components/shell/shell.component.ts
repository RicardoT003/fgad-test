import { Component } from "@angular/core";
import { AppConstantsQuery } from "src/app/core/state/app-constants/query";
import { FooterContactInfo } from "../footer/footer.component";
import { constantsCodes } from "src/app/domain/constants/contants-codes";

@Component({
  selector: "app-shell",
  templateUrl: "./shell.component.html",
  styleUrls: ["./shell.component.scss"]
})
export class ShellComponent {
  footerContactInfo: FooterContactInfo;

  constructor(private appConstantsQuery: AppConstantsQuery) {
    this.footerContactInfo = {
      email: appConstantsQuery.getByCode(constantsCodes.EMAIL_CONTACTO),
      phone: appConstantsQuery.getByCode(constantsCodes.TELEFONO_CONTACTO)
    };
  }
}
