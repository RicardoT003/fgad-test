import { Component, OnInit, Input } from "@angular/core";
import { Menu } from "src/app/domain/models/menu.model";
import { AppMenusService } from "src/app/core/services/app-menus.service";
import { AppMenusQuery } from "src/app/core/state/app-menus/query";
import { tap, filter, map, take } from "rxjs/operators";
import { takeUntilDestroy } from "src/app/lib/helpers/rxjs.helper";

@Component({
  selector: "app-menu-aside",
  templateUrl: "./menu-aside.component.html",
  styleUrls: ["./menu-aside.component.scss"]
})
export class MenuAsideComponent implements OnInit {
  activatedMenuCode: string;
  activatedSubmenuCode: string;
  submenus: Menu[];
  submenusOpenMap: { [submenuCode: string]: boolean } = {};
  submenus2Map: { [submenuCode: string]: Menu[] } = {};

  constructor(
    private appMenuSvc: AppMenusService,
    private appMenuQuery: AppMenusQuery
  ) {
    this.appMenuQuery.activatedMenuCode$
      .pipe(
        takeUntilDestroy(this),
        filter(menuCode => !!menuCode),
        tap((menuCode: any) => (this.activatedMenuCode = menuCode)),
        map((menuCode: any) => this.appMenuQuery.getSubmenus(menuCode)),
        tap(subMenus => (this.submenus = subMenus)),
        tap(() => {
          this.submenus.forEach(submenuCode => {
            this.submenus2Map[
              submenuCode.codigo
            ] = this.appMenuQuery.getSubmenus(submenuCode.codigo);
            if (this.submenusOpenMap[submenuCode.codigo] !== undefined) {
              this.submenusOpenMap[submenuCode.codigo] = false;
            }
          });
        })
      )
      .subscribe();
    this.appMenuQuery.activatedSubMenuCode$
      .pipe(
        takeUntilDestroy(this),
        filter(code => !!code),
        // tap(() => Object.keys(this.submenusOpenMap).forEach(code => this.submenusOpenMap[code] = false)),
        tap((code: any) => (this.submenusOpenMap[code] = true))
      )
      .subscribe();
  }

  ngOnInit() {}
}
