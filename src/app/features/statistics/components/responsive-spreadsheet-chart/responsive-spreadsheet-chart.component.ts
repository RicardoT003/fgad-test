import { Component, OnInit, Input, ChangeDetectionStrategy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as $ from 'jquery';
import { fromEvent } from 'rxjs';
import { tap, delay } from 'rxjs/operators';

@Component({
  selector: 'app-responsive-spreadsheet-chart',
  templateUrl: './responsive-spreadsheet-chart.component.html',
  styleUrls: ['./responsive-spreadsheet-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResponsiveSpreadsheetChartComponent implements OnInit, AfterViewInit {

  @ViewChild('iframeContainer', { static: true }) iframeContainerElm: ElementRef;
  @Input() googleSpreadsheetChartUrl: string;

  constructor(public sanitizer: DomSanitizer, private elmRef: ElementRef) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    if (!this.googleSpreadsheetChartUrl) return;
    const meWidth = $(this.iframeContainerElm.nativeElement).outerWidth(true);
    const meHeight = $(this.iframeContainerElm.nativeElement).outerHeight(true);
    const iframeWidth = $(this.googleSpreadsheetChartUrl).attr('width');
    const iframeHeight = $(this.googleSpreadsheetChartUrl).attr('height');
    const ratio = meWidth / iframeWidth;
    $(this.iframeContainerElm.nativeElement).css('height', iframeHeight * ratio);
    $(this.iframeContainerElm.nativeElement).find('iframe').css('transform', `scale(${ratio})`);
  }


}
