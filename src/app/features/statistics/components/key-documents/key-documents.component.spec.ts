import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyDocumentsComponent } from './key-documents.component';

describe('KeyDocumentsComponent', () => {
  let component: KeyDocumentsComponent;
  let fixture: ComponentFixture<KeyDocumentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyDocumentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyDocumentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
