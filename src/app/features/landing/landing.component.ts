import { Component, OnInit, OnDestroy } from "@angular/core";
import { AppLayoutService } from "src/app/core/services/app-layout.service";
import { AppMenusService } from "src/app/core/services/app-menus.service";

@Component({
  selector: "app-landing",
  templateUrl: "./landing.component.html",
  styleUrls: ["./landing.component.scss"],
})
export class LandingComponent implements OnInit, OnDestroy {
  constructor(
    private appLayoutSvc: AppLayoutService,
    private appMenusService: AppMenusService
  ) {}

  ngOnInit() {
    this.appLayoutSvc.showLandingLayout();
    console.log("xx");
  }

  ngOnDestroy() {
    this.appLayoutSvc.showCommonLayout();
  }
}
