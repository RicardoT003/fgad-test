import { PagesStore } from "../state/pages/store";
import { ApiUtil } from "src/app/core/utils/api.util";
import { Injectable } from "@angular/core";
import { PagesQuery } from "../state/pages/query";
import { of, concat, empty } from "rxjs";
import { catchError, map, tap, flatMap } from "rxjs/operators";
import { Page } from "../models/page.model";
import { PageSection } from "../models/page-section.model";
import { sortBy } from "src/app/lib/helpers/array.helper";

const MAX_SECTIONS_NUMBER = 10;
const MAX_SECTION_CHARTS_NUMBER = 10;

@Injectable({
  providedIn: "root",
})
export class PagesService {
  constructor(
    private api: ApiUtil,
    private pagesStore: PagesStore,
    private pagesQuery: PagesQuery
  ) {}

  loadPage(pageId: string) {
    console.log("ID;::", pageId);

    if (this.pagesQuery.hasEntity(pageId)) {
      return of(true).pipe(tap(() => this.pagesStore.setActive(pageId)));
    }
    return of(true).pipe(
      tap(() => this.pagesStore.setLoading(true)),
      flatMap(() => this.api.get(`/paginas/${pageId}`)),
      map((resp) => {
        const pageSections: PageSection[] = [];
        for (let i = 1; i <= MAX_SECTIONS_NUMBER; i++) {
          const sectionTitle = resp[`seccion${i}Titulo`];
          const sectionImage = resp[`seccion${i}Imagen`];
          const sectionContent = resp[`seccion${i}Contenido`];
          const sectionTipoGrafico = resp[`seccion${i}TipoGrafico`];
          const seccionGraficoSpreadsheetUrl =
            resp[`seccion${i}graficoSpreadsheetUrl`];
          const seccionFuente = resp[`seccion${i}Fuente`];
          const graficos = [];
          if (sectionTitle) {
            if (sectionTipoGrafico === "graficoSpreadsheetMultiple") {
              for (let j = 1; j <= MAX_SECTION_CHARTS_NUMBER; j++) {
                const graficoUrl = `seccion${i}graficoSpreadsheet${j}Url`;
                const graficoNombre = `seccion${i}graficoSpreadsheet${j}Nombre`;
                if (resp[graficoUrl]) {
                  graficos.push({
                    url: resp[graficoUrl],
                    nombre: resp[graficoNombre],
                  });
                }
              }
            }
            pageSections.push(
              new PageSection({
                titulo: sectionTitle,
                contenido: sectionContent,
                imagen: sectionImage,
                tipoGrafico: sectionTipoGrafico,
                graficoSpreadsheetUrl: seccionGraficoSpreadsheetUrl,
                fuente: seccionFuente,
                graficos,
              })
            );
          }
        }
        const page = new Page({
          ...resp,
          secciones: pageSections,
          avancesproyectos: (resp.avancesproyectos || []).sort(sortBy("orden")),
        });
        return page;
      }),
      tap((page) => this.pagesStore.add(page)),
      tap((page) => this.pagesStore.setActive(page.id)),
      catchError((err) => {
        this.pagesStore.setActive(null);
        this.pagesStore.setError(err);
        return of(null);
      }),
      tap(() => this.pagesStore.setLoading(false))
    );
  }
}
