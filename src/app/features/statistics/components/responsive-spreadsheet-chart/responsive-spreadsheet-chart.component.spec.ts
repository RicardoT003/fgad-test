import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsiveSpreadsheetChartComponent } from './responsive-spreadsheet-chart.component';

describe('ResponsiveSpreadsheetChartComponent', () => {
  let component: ResponsiveSpreadsheetChartComponent;
  let fixture: ComponentFixture<ResponsiveSpreadsheetChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponsiveSpreadsheetChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsiveSpreadsheetChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
