import { Injectable } from "@angular/core";
import { ProyectosStore } from "../state/proyectos/store";
import { ProyectosQuery } from "../state/proyectos/query";
import { Observable, of } from "rxjs";
import { ApiUtil } from "src/app/core/utils/api.util";
import { map, flatMap, tap, catchError } from "rxjs/operators";
import { AnniosStore } from '../state/annios/store';
import { AnniosQuery } from '../state/annios/query';

@Injectable({
    providedIn: "root"
})
export class AnniosService {
    constructor(
        private anniosStore: AnniosStore,
        private anniosQuery: AnniosQuery,
        private api: ApiUtil
    ) { }

    loadAnnios(): Observable<boolean> {
        const getAnnios$ = this.api.get("/annios").pipe(
            tap(response => this.anniosStore.set(response)),
            map(() => true),
            catchError(err => {
                console.error(err);
                this.anniosStore.setError(err);
                return of(false);
            })
        );
        return this.anniosQuery
            .selectLoading()
            .pipe(flatMap(loading => (loading ? getAnnios$ : of(true))));
    }
}
