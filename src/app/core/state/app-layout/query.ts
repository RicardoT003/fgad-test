import { Query } from "@datorama/akita";
import { AppLayoutState } from "./state";
import { AppLayoutStore } from "./store";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class AppLayoutQuery extends Query<AppLayoutState> {
  showNavbar$ = this.select(state => state.showNavbar);
  pushNavbar$ = this.select(st => st.pushNavbar);
  showMobileNavbar$ = this.select(st => st.showMobileNavbar);

  constructor(protected appLayoutStore: AppLayoutStore) {
    super(appLayoutStore);
  }
}
