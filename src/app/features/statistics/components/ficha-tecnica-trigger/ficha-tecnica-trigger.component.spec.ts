import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FichaTecnicaTriggerComponent } from './ficha-tecnica-trigger.component';

describe('FichaTecnicaTriggerComponent', () => {
  let component: FichaTecnicaTriggerComponent;
  let fixture: ComponentFixture<FichaTecnicaTriggerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FichaTecnicaTriggerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FichaTecnicaTriggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
