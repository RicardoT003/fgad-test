import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChallengesSelectAndDescriptionComponent } from './challenges-select-and-description.component';

describe('ChallengesSelectAndDescriptionComponent', () => {
  let component: ChallengesSelectAndDescriptionComponent;
  let fixture: ComponentFixture<ChallengesSelectAndDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengesSelectAndDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChallengesSelectAndDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
