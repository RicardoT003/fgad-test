import {
  CanActivate,
  ActivatedRouteSnapshot,
  Router,
  CanActivateChild,
} from "@angular/router";
import { Observable, of } from "rxjs";
import { PagesService } from "../services/pages.service";
import { first, map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { AppMenusQuery } from "src/app/core/state/app-menus/query";
import { sortBy } from "src/app/lib/helpers/array.helper";

@Injectable()
export class LoadPageGuard implements CanActivateChild {
  constructor(
    private pagesSvc: PagesService,
    private menusQuery: AppMenusQuery,
    private router: Router
  ) {}
  canActivateChild(
    route: ActivatedRouteSnapshot
  ): boolean | Observable<boolean> {
    const statisticsPagesPrefix = "/estadisticas";
    const { menuCode, submenuCode, submenu2Code } = route.params;
    console.log(menuCode, submenuCode, submenu2Code);

    const loadPageId = (pageId) =>
      this.pagesSvc.loadPage(pageId).pipe(
        first(),
        map(() => true)
      );
    if (menuCode && !submenuCode && !submenu2Code) {
      const { subMenus } = this.menusQuery.getByCode(menuCode);
      const sortedSubMenus = subMenus.slice().sort(sortBy("orden"));
      const redirectUrl = `${statisticsPagesPrefix}/${menuCode}/${sortedSubMenus[0].codigo}`;
      this.router.navigateByUrl(redirectUrl);
      return false;
    }
    if (menuCode && submenuCode && !submenu2Code) {
      const { subMenus = [], pagina = { id: null } } =
        this.menusQuery.getByCode(submenuCode) || {};
      if (!subMenus || subMenus.length === 0) {
        return loadPageId(pagina.id);
      }
      const sortedSubMenus = subMenus.slice().sort(sortBy("orden"));
      const redirectUrl = `${statisticsPagesPrefix}/${menuCode}/${submenuCode}/${sortedSubMenus[0].codigo}`;
      this.router.navigateByUrl(redirectUrl);
      return false;
    }
    const { pagina = { id: null } } = this.menusQuery.getByCode(submenu2Code);
    return loadPageId(pagina ? pagina.id : null);
  }
}
