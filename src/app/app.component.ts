import { Component, OnInit } from "@angular/core";
import { AppMenusService } from "./core/services/app-menus.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  constructor(private appMenusService: AppMenusService) {}
  ngOnInit(): void {
    this.appMenusService
      .loadMenusOpt()
      .subscribe((val) => console.log("AAA", val));
  }
  title = "angular-web-seed";
}
