import { NgModule } from "@angular/core";
import { MenuAsideComponent } from "./menu-aside/menu-aside.component";
import { AppSharedModule } from "src/app/shared/shared.module";
import { ShellComponent } from './shell/shell.component';
import { RouterModule } from '@angular/router';
import { StrategicCommitteeComponent } from './strategic-committee/strategic-committee.component';
import { KeyDocumentsComponent } from './key-documents/key-documents.component';
import { ResponsiveSpreadsheetChartComponent } from './responsive-spreadsheet-chart/responsive-spreadsheet-chart.component';
import { FichaTecnicaTriggerComponent } from './ficha-tecnica-trigger/ficha-tecnica-trigger.component';
import { FichaTecnicaModalComponent } from './ficha-tecnica-modal/ficha-tecnica-modal.component';
import { MenuSelectComponent } from './menu-select/menu-select.component';
import { ChallengesTableComponent } from './challenges-table/challenges-table.component';
import { ChallengesSelectAndDescriptionComponent } from './challenges-select-and-description/challenges-select-and-description.component';
import { MacroproyectosTableComponent } from './macroproyectos-table/macroproyectos-table.component';
import { AvancesTableComponent } from './avances-table/avances-table.component';

@NgModule({
  declarations: [MenuAsideComponent, ShellComponent, StrategicCommitteeComponent, KeyDocumentsComponent, ResponsiveSpreadsheetChartComponent, FichaTecnicaTriggerComponent, FichaTecnicaModalComponent, MenuSelectComponent, ChallengesTableComponent, ChallengesSelectAndDescriptionComponent, MacroproyectosTableComponent, AvancesTableComponent,],
  imports: [AppSharedModule, RouterModule],
  exports: [MenuAsideComponent, ShellComponent, StrategicCommitteeComponent, KeyDocumentsComponent, ResponsiveSpreadsheetChartComponent, FichaTecnicaTriggerComponent, FichaTecnicaModalComponent, MenuSelectComponent, ChallengesTableComponent, ChallengesSelectAndDescriptionComponent, MacroproyectosTableComponent, AvancesTableComponent,],
  entryComponents: [
    FichaTecnicaModalComponent
  ]
})
export class ComponentsModule { }
