import { EntityState } from '@datorama/akita';
import { Annio } from '../../models/annio.model';

export interface AnniosState extends EntityState<Annio> {

}