import { QueryEntity, QueryConfig, Order } from '@datorama/akita';
import { AnniosState } from './state';
import { AnniosStore } from './store';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
@QueryConfig({
    sortBy: 'valor',
    sortByOrder: Order.ASC
})
export class AnniosQuery extends QueryEntity<AnniosState> {
    constructor(protected store: AnniosStore) {
        super(store);
    }
}