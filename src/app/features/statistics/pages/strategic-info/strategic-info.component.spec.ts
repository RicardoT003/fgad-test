import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategicInfoComponent } from './strategic-info.component';

describe('StrategicInfoComponent', () => {
  let component: StrategicInfoComponent;
  let fixture: ComponentFixture<StrategicInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategicInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
