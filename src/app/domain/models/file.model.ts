import { BaseModel } from "./base/base.model";

export class FileModel extends BaseModel<FileModel> {
  file: Blob;
  url: string;
}
