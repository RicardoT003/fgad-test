import { Injectable } from "@angular/core";
import { ApiUtil } from "../utils/api.util";
import { AppSessionStore } from "../state/app-session/store";

@Injectable({
  providedIn: "root"
})
export class AppSessionService {
  constructor(private api: ApiUtil, private appSessionStore: AppSessionStore) { }
  populateSession() { }
  purgeSession() {
    this.appSessionStore.reset();
  }
}
