import { NgModule } from "@angular/core";

import { AuthenticationRoutingModule } from "./authentication-routing.module";
import { AuthenticationComponent } from "./authentication.component";
import { AuthenticationService } from "./services/authentication.service";

@NgModule({
  providers: [AuthenticationService],
  declarations: [AuthenticationComponent],
  imports: [AuthenticationRoutingModule]
})
export class AuthenticationModule {}
