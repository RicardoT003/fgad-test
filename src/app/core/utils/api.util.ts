import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { AppSessionQuery } from "../state/app-session/query";

const TOKEN_TYPE = "Bearer";

export class ApiOptions {
  params?: HttpParams;
  headers?: HttpHeaders;
  observe?: string;
  isAuthenticated?: boolean = true;
}

@Injectable()
export class ApiUtil {
  private backendUrl: string;

  constructor(private sessionQuery: AppSessionQuery, public http: HttpClient) {
    this.backendUrl = environment.backendUrl;
  }

  private appendAuthorizationHeader(headers: HttpHeaders): HttpHeaders {
    headers = headers || new HttpHeaders();
    let token = this.sessionQuery.token;
    if (token && token != "") {
      headers = headers.append(
        "Authorization",
        `${TOKEN_TYPE} ${this.sessionQuery.token}`
      );
    }
    return headers;
  }

  public get(
    path: string,
    options: ApiOptions = new ApiOptions()
  ): Observable<any> {
    options = options || {};
    options.headers = options.isAuthenticated
      ? this.appendAuthorizationHeader(options.headers)
      : options.headers;
    return this.http.get(`${this.backendUrl}${path}`, {
      params: options.params,
      headers: options.headers,
      observe: (options.observe || "body") as "body",
    });
  }

  public post(
    path: string,
    body?: any,
    options: ApiOptions = new ApiOptions()
  ): Observable<any> {
    options = options || {};
    options.headers = options.isAuthenticated
      ? this.appendAuthorizationHeader(options.headers)
      : options.headers;
    return this.http.post(`${this.backendUrl}${path}`, body, {
      params: options.params,
      headers: options.headers,
      observe: (options.observe || "body") as "body",
    });
  }

  public put(
    path: string,
    body?: any,
    options: ApiOptions = new ApiOptions()
  ): Observable<any> {
    options = options || {};
    options.headers = this.appendAuthorizationHeader(options.headers);
    return this.http.put(`${this.backendUrl}${path}`, body, {
      params: options.params,
      headers: options.headers,
      observe: (options.observe || "body") as "body",
    });
  }

  public patch(
    path: string,
    body?: any,
    options: ApiOptions = new ApiOptions()
  ): Observable<any> {
    options = options || {};
    options.headers = this.appendAuthorizationHeader(options.headers);
    return this.http.patch(`${this.backendUrl}${path}`, body, {
      params: options.params,
      headers: options.headers,
      observe: (options.observe || "body") as "body",
    });
  }

  public delete(
    path: string,
    options: ApiOptions = new ApiOptions()
  ): Observable<any> {
    options = options || {};
    options.headers = this.appendAuthorizationHeader(options.headers);
    return this.http.delete(`${this.backendUrl}${path}`, {
      params: options.params,
      headers: options.headers,
      observe: (options.observe || "body") as "body",
    });
  }
}
