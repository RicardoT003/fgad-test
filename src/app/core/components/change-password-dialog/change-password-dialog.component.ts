import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/features/authentication/services/authentication.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
  styleUrls: ['./change-password-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChangePasswordDialogComponent implements OnInit {

  changePasswordFG: FormGroup;
  _error: string;
  set error(val) {
    this._error = val;
    this.changeDetectorRef.detectChanges();
  }
  get error() {
    return this._error;
  }

  constructor(public fb: FormBuilder, private changeDetectorRef: ChangeDetectorRef,
    private authService: AuthenticationService, private dialogRef: MatDialogRef<ChangePasswordDialogComponent>) {
    this.changePasswordFG = this.fb.group({
      currentPassword: [null, [Validators.required]],
      newPassword: [null, [Validators.required]],
      confirmNewPassword: [null, [Validators.required]]
    });
  }

  ngOnInit() {
  }

  handleSubmit() {
    if ((this.changePasswordFG.controls.currentPassword.errors || {}).required) {
      this.error = 'Debe ingresar la nueva contrseña';
      return;
    }
    if ((this.changePasswordFG.controls.newPassword.errors || {}).required) {
      this.error = 'La nueva contraseña no puede ser vacía';
      return;
    }
    const { currentPassword, newPassword, confirmNewPassword } = this.changePasswordFG.value;
    if (newPassword !== confirmNewPassword) {
      this.error = 'Las contraseñas no coinciden';
      return;
    } else {
      this.error = null;
    }
    this.authService.changePassword({
      currentPassword,
      newPassword
    })
      .subscribe({
        next: response => {
          if (response !== true) {
            this.error = response;
          } else {
            this.error = null;
            this.dialogRef.close();
          }
        }
      });
  }

}
